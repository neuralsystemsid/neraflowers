import 'package:nera_flowers/screens/web_view.dart';
import 'package:nera_flowers/screens/splash.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';

FirebaseAnalytics analytics = FirebaseAnalytics();

var routes = <String, WidgetBuilder>{
  "/home": (BuildContext context) => WebViewWidget(),
  "/intro": (BuildContext context) => SplashScreen(),
};

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.amber, accentColor: Colors.white),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      routes: routes,
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }
}
