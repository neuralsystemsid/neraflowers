import 'package:nera_flowers/model/web_page.dart';

List<WebPage> webPages = [
  WebPage(title: 'Hit', url: 'https://neraflowers.com/hit'),
  WebPage(title: 'Naslovnica', url: 'https://neraflowers.com'),
  WebPage(title: 'O nama', url: 'https://neraflowers.com/o-nama'),
  WebPage(
      title: 'Račun',
      url: 'https://neraflowers.com/moj-profil?view=login'),
  WebPage(
      title: 'Kalendar',
      url: 'https://neraflowers.com/index.php/kalendar-dogadanja'),
  WebPage(title: 'Kontakt', url: 'https://neraflowers.com/kontakt'),
];
