import 'dart:async';

import 'package:nera_flowers/data/data.dart';
import 'package:nera_flowers/model/web_page.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'dart:io';

class WebViewWidget extends StatefulWidget {
  @override
  WebViewWidgetState createState() => WebViewWidgetState();
}

class WebViewWidgetState extends State<WebViewWidget> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  GlobalKey _bottomNavigationKey = GlobalKey();
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  int currentPageIndex;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    currentPageIndex = 0;
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final WebPage currentPage = webPages[currentPageIndex];

    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "NeraFlowers",
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                  Text(
                    currentPage.title,
                    style: TextStyle(color: Colors.white, fontSize: 14.0),
                  ),
                ]),
            automaticallyImplyLeading: false,
            /* actions: <Widget>[
          new Container(width: 60.0, height: 20.0, color: Colors.blue,
             child:  CircularProgressIndicator( backgroundColor: Colors.white),)]*/
          ),
          body: SafeArea(
            child: Stack(
              children: <Widget>[
                WebView(
                  initialUrl: currentPage.url,
                  userAgent: 'NeraAPP',
                  javascriptMode: JavascriptMode.unrestricted,
                  onPageFinished: pageFinishedLoading,
                  onWebViewCreated: _controller.complete,
                ),
                (_isLoading || _connectionStatus == 'ConnectivityResult.none')
                    ? Container(
                        decoration: new BoxDecoration(color: Colors.white),
                        child: new Center(
                          child: LoadingBouncingGrid.square(
                            borderColor: Colors.white,
                            borderSize: 2.0,
                            size: 50,
                            backgroundColor: Colors.amber,
                          ),
                        ),
                      )
                    : Center(),
              ],
            ),
          ),
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            index: 0,
            height: 50.0,
            items: <Widget>[
              Icon(Icons.home, size: 30, color: Colors.white),
              Icon(Icons.shopping_basket, size: 30, color: Colors.white),
              Icon(Icons.info, size: 30, color: Colors.white),
              Icon(Icons.account_circle, size: 30, color: Colors.white),
              Icon(Icons.calendar_today, size: 30, color: Colors.white),
              Icon(Icons.help, size: 30, color: Colors.white),
            ],
            color: Colors.amber,
            buttonBackgroundColor: Colors.amber,
            backgroundColor: Colors.white,
            animationCurve: Curves.easeInOut,
            animationDuration: Duration(milliseconds: 600),
            onTap: onTapNavigation,
          ),
        ));
  }

  void pageFinishedLoading(String url) {
    setState(() {
      _isLoading = false;
    });
  }

  void onTapNavigation(int index) {
    setState(() {
      _isLoading = true;
      currentPageIndex = index;
    });

    final WebPage currentPage = webPages[currentPageIndex];

    _controller.future.then((WebViewController controller) {
      controller.loadUrl(currentPage.url);
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return;
    }

    _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
        String wifiName, wifiBSSID, wifiIP;

        try {
          if (Platform.isIOS) {
            LocationAuthorizationStatus status =
                await _connectivity.getLocationServiceAuthorization();
            if (status == LocationAuthorizationStatus.notDetermined) {
              status =
                  await _connectivity.requestLocationServiceAuthorization();
            }
            if (status == LocationAuthorizationStatus.authorizedAlways ||
                status == LocationAuthorizationStatus.authorizedWhenInUse) {
              wifiName = await _connectivity.getWifiName();
            } else {
              wifiName = await _connectivity.getWifiName();
            }
          } else {
            wifiName = await _connectivity.getWifiName();
          }
        } on PlatformException catch (e) {
          print(e.toString());
          wifiName = "Failed to get Wifi Name";
        }

        try {
          if (Platform.isIOS) {
            LocationAuthorizationStatus status =
                await _connectivity.getLocationServiceAuthorization();
            if (status == LocationAuthorizationStatus.notDetermined) {
              status =
                  await _connectivity.requestLocationServiceAuthorization();
            }
            if (status == LocationAuthorizationStatus.authorizedAlways ||
                status == LocationAuthorizationStatus.authorizedWhenInUse) {
              wifiBSSID = await _connectivity.getWifiBSSID();
            } else {
              wifiBSSID = await _connectivity.getWifiBSSID();
            }
          } else {
            wifiBSSID = await _connectivity.getWifiBSSID();
          }
        } on PlatformException catch (e) {
          print(e.toString());
          wifiBSSID = "Failed to get Wifi BSSID";
        }

        try {
          wifiIP = await _connectivity.getWifiIP();
        } on PlatformException catch (e) {
          print(e.toString());
          wifiIP = "Failed to get Wifi IP";
        }

        setState(() {
          _connectionStatus = '$result\n'
              'Wifi Name: $wifiName\n'
              'Wifi BSSID: $wifiBSSID\n'
              'Wifi IP: $wifiIP\n';
        });
        break;
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        setState(() => _connectionStatus = result.toString());
        break;
      default:
        setState(() => _connectionStatus = 'Failed to get connectivity.');
        break;
    }
  }
}
